using Godot;
using System;

public class Main : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    Mushrum player;
    Room currentRoom;
    // Called when the node enters the scene tree for the first time.
    AcceptDialog alertBox;
    bool welcomeMessageShown = false;
    public override void _Ready() {
        player = GetNode<Mushrum>("Mushrum");
        RoomManager.instance.main = this;
        
        MoveToRoom(RoomManager.instance.GetRoomForCoordinates(0,0), ExitDirection.Left);
        alertBox = GetNode<AcceptDialog>("CanvasLayer/CenterContainer/AcceptDialog");
    }

    public void MoveToRoom(Room room, ExitDirection direction) {
        if (!(currentRoom is null)) {
            currentRoom.QueueFree();
        }
        
        AddChild(room);
        MoveChild(room,0);

        player.hasTouchedGround = (direction == ExitDirection.Top);
        player.Position = room.GetNode<Node2D>(direction.ToString() + "Entrance").Position;
        player.checkpointPosition = player.Position;
        currentRoom = room;
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        if (!welcomeMessageShown) {
            alertBox.WindowTitle = "Welcome to the ruins.";
            alertBox.DialogText = "You are a mushroom explorer searching the ruins for tablets to decode an ancient text.\nEach tablet has the information to decipher one letter.\nPress H to view your progress at any time.";
            alertBox.Popup_();
            welcomeMessageShown = true;
        }

        if (Input.IsActionJustReleased("ui_popup")) {
            var lettersGuessedString = RuneManager.instance.hangman.LettersGuessedCount();
            alertBox.WindowTitle = lettersGuessedString + " letters known.";
            alertBox.DialogText = "You have discovered " + lettersGuessedString + " letters.\nThe message known so far is:\n" + RuneManager.instance.hangman.KnownLetters(); 
            alertBox.Popup_();
        }
    }

    public void Victory() {
        alertBox.WindowTitle = "Congratulations!";
        alertBox.DialogText = "You have deciphered all 26 letters!" + ".\n The message is:" + RuneManager.instance.hangman.KnownLetters(); 
        alertBox.Popup_();
    }

    public void _OnAcceptDialogAboutToShow() {
        GetTree().Paused = true;
    }

    public void _OnAcceptDialogPopupHide() {
        GetTree().Paused = false;
    }    

}
