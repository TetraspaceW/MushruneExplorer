using System;
using System.Text.RegularExpressions;
using Godot;
public class RuinsPhraseGenerator : Node {
    private string[] phrases;
    Random rand = new Random();
    public RuinsPhraseGenerator() {
        string fullText = MobyDick.text;
        Regex rgx = new Regex("[^A-Za-z0-9 \\.]");
        fullText = rgx.Replace(fullText, "").ToLower();
        phrases = fullText.Split(".");
    }

    public string RandomPhrase() {
        if (phrases.Length > 0) {
            return phrases[rand.Next(phrases.Length)];
        } else {
            return "The quick brown fox jumped over the lazy dog";
        }
    }
}