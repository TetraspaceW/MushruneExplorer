using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Godot;
public class Hangman {
    private string phraseToGuess;
    private List<char> lettersGuessed = new List<char>();
    public Hangman(string phraseToGuess) {
        this.phraseToGuess = phraseToGuess;
    }

    public LetterMatch GuessLetter(char letter) {
        if (lettersGuessed.Contains(letter)) {
            return LetterMatch.AlreadyGuessed;
        } else {
            lettersGuessed.Add(letter);
            GD.Print(KnownLetters());
            if (phraseToGuess.Contains(letter)) {
                return LetterMatch.Correct;
            } else {
                return LetterMatch.Incorrect;
            }
        }
    }

    public string KnownLetters() {
        if (lettersGuessed.Count() > 0) {
            Regex rgx = new Regex("[^" + new string(lettersGuessed.ToArray()) + " ]");
            return rgx.Replace(phraseToGuess,"*");
        } else {
            Regex rgx = new Regex("[^ ]");
            return rgx.Replace(phraseToGuess,"*");
        }
        
    }

    public string LettersGuessedCount() {
        return lettersGuessed.Count().ToString();
    }

    public enum LetterMatch {
        Correct,
        Incorrect,
        AlreadyGuessed
    }

}