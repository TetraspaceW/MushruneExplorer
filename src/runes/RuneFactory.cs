using System;
using Godot;
using System.Collections.Generic;
using System.Linq;
public class RuneFactory {
    private char[] alphabet;
    public Dictionary<char, RuneModel> runes = new Dictionary<char, RuneModel>();
    public RuneFactory(char[] alphabet) {
        this.alphabet = alphabet;
        foreach (var letter in alphabet)
        {
            runes.Add(letter, GenerateUniqueRuneFor(letter));
        }
    }

    private RuneModel GenerateUniqueRuneFor(char letter) {
        Random rnd = new Random();
        bool[] segments = new bool[RuneConstants.numberOfSegments];

        do {
            for (int segment = 0; segment < RuneConstants.numberOfSegments; segment++) {
                segments[segment] = (rnd.Next(2) == 1);
            }
        } while( runes.Values.Any(rune => rune.segments.SequenceEqual(segments)) );

        RuneModel runeModel = new RuneModel(segments, letter);
        return runeModel;
    }
}