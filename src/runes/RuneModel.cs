using Godot;
using System;

public class RuneModel
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    bool[] _segments = new bool[RuneConstants.numberOfSegments];
    char letter;

    // Called when the node enters the scene tree for the first time.
    public RuneModel(bool[] segments, char letter) {
        this.letter = letter;
        Random random = new Random();
        this._segments = segments;
    }
    public bool[] segments { get => _segments; }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
