using Godot;
using System;

public class Mushrum : KinematicBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    float xVelocity = 0;
    float yVelocity = 0;
    float speed = 500;
    AnimatedSprite sprite;
    AudioStreamPlayer walkAudioStreamPlayer;
    AudioStreamPlayer jumpAudioStreamPlayer;
    public bool hasTouchedGround = false;
    public Vector2 checkpointPosition;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        checkpointPosition = Position;
        sprite = GetNode<AnimatedSprite>("Sprite");
        walkAudioStreamPlayer = GetNode<AudioStreamPlayer>("WalkAudioStreamPlayer");
        jumpAudioStreamPlayer = GetNode<AudioStreamPlayer>("JumpAudioStreamPlayer");
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
    private void HandleInput() {
        xVelocity = 0;
        if (Input.IsActionPressed("ui_left")) {
            xVelocity -= 1;
        }
        if (Input.IsActionPressed("ui_right")) {
            xVelocity += 1;
        }

        if (xVelocity != 0) {
            xVelocity = xVelocity / Math.Abs(xVelocity) * speed;
        } else {
            xVelocity = 0;
        }
    }
    public override void _PhysicsProcess(float delta) {
        HandleInput();
        yVelocity += PhysicsConstants.gravity * delta;
        Vector2 velocity = new Vector2(xVelocity, yVelocity);
        
        MoveAndSlide(velocity, PhysicsConstants.upDirection, true);

        if (IsOnFloor()) {
            if (Input.IsActionPressed("ui_up")) {
                yVelocity = -(float)Math.Sqrt(PhysicsConstants.gravity) * (float)Math.Sqrt(1000);
            } else {
                yVelocity = 0;
            }
        }

        if (IsOnCeiling()) {
            yVelocity = 0;
        }

        SetGraphics();
    }

    private void SetGraphics() {
        var previousAnimation = sprite.Animation;

        if (xVelocity > 0) {
            sprite.FlipH = false;
        } else if (xVelocity < 0) {
            sprite.FlipH = true;
        }

        if (sprite.Animation == "land" || sprite.Animation == "crest") {
            return;
        }

        if (yVelocity > 0 && previousAnimation == "jump") {
            sprite.Animation = "crest";
            return;
        }

        if (yVelocity == 0) {
            if (xVelocity > 0) {
                sprite.FlipH = false;
                sprite.Animation = "walk";
                walkAudioStreamPlayer.StreamPaused = false;
            } else if (xVelocity < 0) {
                sprite.FlipH = true;
                sprite.Animation = "walk";
                walkAudioStreamPlayer.StreamPaused = false;
            } else {
                sprite.Animation = "idle";
                walkAudioStreamPlayer.StreamPaused = true;
            }
        }

        if (yVelocity < 0) {
            sprite.Animation = "jump";
        }

        if (yVelocity > 0) {
            sprite.Animation = "fall";
        }

        if (IsOnFloor() && previousAnimation == "fall") {
            sprite.Animation = "land";
        }

        if (!IsOnFloor()) {
            walkAudioStreamPlayer.StreamPaused = true;
        }
    }

    public void _OnSpriteAnimationFinished() {
        if (sprite.Animation == "land") {
            if (hasTouchedGround) {
                jumpAudioStreamPlayer.Play(0);
            } else {
                hasTouchedGround = true;
            }
            sprite.Animation = "idle";
        }
        if (sprite.Animation == "crest") {
            sprite.Animation = "fall";
        }
    }

    public void HandleBeingKilled() {
        Position = checkpointPosition;
        yVelocity = 0;
        xVelocity = 0;
    }
}
