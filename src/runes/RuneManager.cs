using System;
using System.Collections.Generic;
using Godot;
using System.Linq;
public class RuneManager {
    public static RuneManager instance  = new RuneManager();
    public RuneFactory runeFactory = new RuneFactory(RuneConstants.alphabet);
    public List<char> ownedRunes = new List<char>();
    public Hangman hangman = new Hangman(new RuinsPhraseGenerator().RandomPhrase());
    public void CollectRuneForLetter(char letter) {
        ownedRunes.Add(letter);
        hangman.GuessLetter(letter);
    }
}