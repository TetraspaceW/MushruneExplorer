using Godot;
using System;

public class HangmanTablet : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    PackedScene segmentSpriteScene;
    string segmentSpritePath = "res://src/hangman/ui/HangmanTabletSegmentSprite.tscn";
    [Export] String letter;
    public char character {
        get {
            return letter[0];
        }
        set {
            letter = value.ToString();
        }
    }

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        segmentSpriteScene = ResourceLoader.Load<PackedScene>(segmentSpritePath);

        var model = RuneManager.instance.runeFactory.runes[character];

        for (int segment = 0; segment < RuneConstants.numberOfSegments; segment++)
        {
            RuneSegmentSprite segmentSprite = (RuneSegmentSprite)segmentSpriteScene.Instance();
            segmentSprite.segmentID = segment;
            segmentSprite.Visible = model.segments[segment];
            AddChild(segmentSprite);
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
