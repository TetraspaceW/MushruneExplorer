public enum ExitDirection {
    Top,
    Bottom,
    Left,
    Right,
}