using Godot;
using System.Collections.Generic;
public class Room: TileMap {
    // Rooms need some number of RoomExits (LeftExit, RightExit, TopExit, BottomExit)
    public RoomModel model = new RoomModel();
    public (int, int) coordinates = (0,0);
    public string name = "";
    Image img;
    int wait = 30;
    public PackedScene runeScene;
    public override void _Ready()
    {
        // var size = new Vector2(GetViewport().Size.x*3.5f, GetViewport().Size.y*5f);
        // GetViewport().Size = size;

        runeScene = ResourceLoader.Load<PackedScene>("res://src/runes/Tablet.tscn");

        if (model.tabletCollected == false) {
            Tablet tablet = (Tablet)runeScene.Instance();
            tablet.character = model.tablet;
            tablet.Position = GetNode<Node2D>("TabletSite").Position;
            AddChild(tablet);
        }
    }

    public override void _Process(float delta)
    {        
        // wait -= 1;
        // if (wait == 15) {
        //     img = GetViewport().GetTexture().GetData();
        // }
        // if (wait == 0) {
        //     img.SavePng("room.png");
        // }
    }
}