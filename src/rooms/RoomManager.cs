using Godot;
using System.Collections.Generic;
using System.Linq;
using System;
public class RoomManager : Node
{
    public static RoomManager instance = new RoomManager();
    static int numberOfRooms = 10;
    Dictionary<int,Dictionary<int,RoomModel>> map = new Dictionary<int,Dictionary<int,RoomModel>>();
    List<PackedScene> roomScenes;
    public Main main;
    public RoomManager() {
        roomScenes = ListOfRooms();
    }
    public Dictionary<ExitDirection,bool> GetRoomExitDirections(Room room) {
        Dictionary<ExitDirection,bool> _exitDirections = new Dictionary<ExitDirection,bool>();
        _exitDirections.Add(ExitDirection.Top, room.HasNode("TopExit"));
        _exitDirections.Add(ExitDirection.Bottom, room.HasNode("BottomExit"));
        _exitDirections.Add(ExitDirection.Left, room.HasNode("LeftExit"));
        _exitDirections.Add(ExitDirection.Right, room.HasNode("RightExit"));
        return _exitDirections;
    }

    public bool GetRoomExitDirection(RoomModel roomModel, ExitDirection exitDirection) {
        var room = roomModel.scene.Instance();
        switch (exitDirection) {
            case ExitDirection.Top: return room.HasNode("TopExit");
            case ExitDirection.Bottom: return room.HasNode("BottomExit");
            case ExitDirection.Left: return room.HasNode("LeftExit");
            case ExitDirection.Right:  return room.HasNode("RightExit");
        }
        return false;
    }
    public Room GetRoomForCoordinates(int x, int y) {
        if (map.ContainsKey(x)) {
            if (map[x].ContainsKey(y)) {
                Room room = (Room)map[x][y].scene.Instance();
                room.coordinates = (x,y);
                room.model = map[x][y];
                return room;
            }
        }
        return GenerateRoomForCoordinates(x, y);
    }

    private Room GenerateRoomForCoordinates(int x, int y)
    {
        // oof
        var validRoomScenes = ListOfRooms();
        GD.Print(validRoomScenes.Count() + " rooms found and loaded.");
        try {
            var filter = GetRoomExitDirection(map[x][y + 1], ExitDirection.Top);
            GD.Print("Bottom exits needed:" + filter);
            validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("BottomExit") == filter);
            GD.Print("Bottom exits filtered, " + validRoomScenes.Count() + " possible rooms remain.");
        } catch { }
        try {
            var filter = GetRoomExitDirection(map[x][y - 1], ExitDirection.Bottom);
            // var filter = GetRoomExitDirections((Room)map[x][y + 1].Instance())[ExitDirection.Top];
            GD.Print("Top exits needed:" + filter);
            validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("TopExit") == filter);
            GD.Print("Top exits filtered, " + validRoomScenes.Count() + " possible rooms remain.");
        } catch { }
        try {
            var filter = GetRoomExitDirection(map[x - 1][y], ExitDirection.Right);
            // var filter = GetRoomExitDirections((Room)map[x - 1][y].Instance())[ExitDirection.Right];
            GD.Print("Left exits needed:" + filter);
            validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("LeftExit") == filter);
            GD.Print("Left exits filtered, " + validRoomScenes.Count() + " possible rooms remain.");
        } catch { }
        try {
            var filter = GetRoomExitDirection(map[x + 1][y], ExitDirection.Left);
            // var filter = GetRoomExitDirections((Room)map[x + 1][y].Instance())[ExitDirection.Left];
            GD.Print("Right exits needed:" + filter);
            validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("RightExit") == filter);
            GD.Print("Right exits filtered, " + validRoomScenes.Count() + " possible rooms remain.");
        } catch { }

        if (x == 0) {
            validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("BottomExit") == true);
            if (y == 0) {
                validRoomScenes = validRoomScenes.FindAll(scene => scene.Instance().HasNode("LeftExit") == true);
            }
        }

        if (validRoomScenes.Count == 0) {
            GD.Print("Something went wrong!");
        }

        Random rnd = new Random();
        var newRoomScene = validRoomScenes[rnd.Next(validRoomScenes.Count())];
        Room newRoom = (Room)newRoomScene.Instance();
        newRoom.coordinates = (x, y);
        newRoom.name = newRoomScene.ResourcePath;
        
        RoomModel newRoomModel = new RoomModel(newRoomScene);

        if (newRoom.HasNode("TabletSite")) {
            newRoomModel.tabletCollected = false;
        }

        newRoom.model = newRoomModel;
        
        if (map.ContainsKey(x)) {
            map[x].Add(y,newRoomModel);
        } else {
            var newRow = new Dictionary<int, RoomModel>();
            newRow.Add(y,newRoomModel);
            map.Add(x,newRow);
        }

        return newRoom;
    }

    private List<PackedScene> ListOfRooms() {
        List<PackedScene> _roomScenes = new List<PackedScene>();
        for (int i = 0; i < numberOfRooms; i++) {
            var roomScene = (PackedScene)ResourceLoader.Load("res://src/rooms/Room" + i + ".tscn");
            _roomScenes.Add(roomScene);
        }
        return _roomScenes;
    }
    public void MoveToRoom(Room room, ExitDirection direction) {
        main.MoveToRoom(room, direction);
    }

    public void CollectTabletInRoom(int x, int y) {
        RoomModel updatedModel = new RoomModel(map[x][y].scene);
        updatedModel.tabletCollected = true;
        map[x][y] = updatedModel;
    }
}
