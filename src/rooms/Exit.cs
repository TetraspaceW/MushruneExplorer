using Godot;
using System;
using System.Linq;
public class Exit : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    [Export] ExitDirection direction;
    bool hasJustEnteredRoom = true;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

    public override void _PhysicsProcess(float delta)
    {
        if (hasJustEnteredRoom) {
            hasJustEnteredRoom = false;
            return;
        }

        Room room = (Room)GetParent();
        Room newRoom = null;
        ExitDirection newDirection = new ExitDirection();
        int newX = room.coordinates.Item1;
        int newY = room.coordinates.Item2;
        switch (direction) {
            case ExitDirection.Top:
                newDirection = ExitDirection.Bottom;
                newY--;
                break;
            case ExitDirection.Bottom:
                newDirection = ExitDirection.Top;
                newY++;
                break;
            case ExitDirection.Left:
                newDirection = ExitDirection.Right;
                newX--;
                break;
            case ExitDirection.Right:
                newDirection = ExitDirection.Left;
                newX++;
                break;
        }

        foreach (var _ in GetOverlappingBodies().OfType<PhysicsBody2D>().Where(body => body is Mushrum).Select(body => new { })) {
            newRoom = RoomManager.instance.GetRoomForCoordinates(newX, newY);
            GD.Print("In room" + room.coordinates + ", moving to room" + newRoom.coordinates + ".");
            GD.Print("Moving" + direction);
            RoomManager.instance.MoveToRoom(newRoom, newDirection);
            hasJustEnteredRoom = true;
            break;
        }        
    }
}
