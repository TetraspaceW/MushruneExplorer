using Godot;
using System.Collections.Generic;
using System.Linq;
using System;
public struct RoomModel {
    public PackedScene scene;
    public char tablet;
    public bool? tabletCollected;

    public RoomModel(PackedScene scene) {
        this.scene = scene;

        var alphabet = new List<char>(RuneConstants.alphabet);
        var ownedRunes = RuneManager.instance.ownedRunes;
        var unownedRunes = alphabet.FindAll(letter => !ownedRunes.Any(rune => rune == letter));

        Random rnd = new Random();

        if (unownedRunes.Count() > 0) {
            this.tablet = unownedRunes[rnd.Next(unownedRunes.Count())];
            this.tabletCollected = null;
        } else {
            this.tablet = 'm';
            this.tabletCollected = true;
        }
    }
}