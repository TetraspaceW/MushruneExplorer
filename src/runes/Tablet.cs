using Godot;
using System;
using System.Linq;

public class Tablet : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    PackedScene segmentSpriteScene;
    string segmentSpritePath = "res://src/runes/RuneSegmentSprite.tscn";
    [Export] String letter;
    public char character {
        get {
            return letter[0];
        }
        set {
            letter = value.ToString();
        }
    }
    RuneModel model;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        segmentSpriteScene = ResourceLoader.Load<PackedScene>(segmentSpritePath);

        var model = RuneManager.instance.runeFactory.runes[character];

        for (int segment = 0; segment < RuneConstants.numberOfSegments; segment++)
        {
            RuneSegmentSprite segmentSprite = (RuneSegmentSprite)segmentSpriteScene.Instance();
            segmentSprite.segmentID = segment;
            segmentSprite.Visible = model.segments[segment];
            AddChild(segmentSprite);
        }
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

    public override void _PhysicsProcess(float delta)
    {
        foreach (var _ in from body in GetOverlappingBodies().OfType<Mushrum>()
                          select new { }) {
            RuneManager.instance.CollectRuneForLetter(character);
            RoomManager.instance.CollectTabletInRoom(GetParent<Room>().coordinates.Item1, GetParent<Room>().coordinates.Item2);
            QueueFree();
            break;
        }
    }
}
